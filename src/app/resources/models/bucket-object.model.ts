export class BucketObject {
  name: string;
  last_modified: Date;
  size: number;
}
