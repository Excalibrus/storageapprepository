import { Location } from './location.model';

export class Bucket {
  id: string;
  name: string;
  location: Location;
}
