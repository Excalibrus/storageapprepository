import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'alert',
  templateUrl: './alert.component.html'
})
export class AlertComponent implements OnInit {
  messageText: string;
  secondsToClose: number;

  constructor(private alertService: AlertService) {
    this.secondsToClose = 5; // 0 = don't close
  }

  ngOnInit() {
      this.alertService.getMessage().subscribe(message => {
        if(message && message.text && message.text.length > 0) {
          this.messageText = message.text;
        }
      });
      if(this.secondsToClose > 0) {
        this.alertService.getMessage().debounceTime(this.secondsToClose * 1000).subscribe(message =>
        {
          this.closeAlert();
        });
      }
  }

  closeAlert() {
    this.messageText = null;
  }

}
