import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { HttpService } from './http.service';
import { Bucket, BucketObject, Location } from '../models/index';


@Injectable()
export class BucketsService {
  private serverUrl: string;
  private useProxy: boolean;

  constructor(
    private httpService: HttpService
  ) {
    this.useProxy = true;
    this.serverUrl = this.useProxy ? "/api" : "https://challenge.3fs.si/storage";
  }

  getLocations(): Observable<Location[]> {
    return this.httpService.get(`${this.serverUrl}/locations`)
    .map(response => <Location[]>response.json().locations);
  }

  getBuckets(): Observable<Bucket[]> {
    return this.httpService.get(`${this.serverUrl}/buckets`)
    .map(response => <Bucket[]>response.json().buckets);
  }

  saveBucket(name: string, location: string): Observable<any> {
    return this.httpService.post(`${this.serverUrl}/buckets`, { name: name, location: location})
    .map(response => response.json());
  }

  deleteBucket(bucketId: string): Observable<any> {
    return this.httpService.delete(`${this.serverUrl}/buckets/${bucketId}`);
  }

  getBucket(bucketId: string): Observable<Bucket> {
    return this.httpService.get(`${this.serverUrl}/buckets/${bucketId}`)
    .map(response => <Bucket>response.json().bucket);
  }

  getBucketObjects(bucketId: string): Observable<BucketObject[]> {
    return this.httpService.get(`${this.serverUrl}/buckets/${bucketId}/objects`)
    .map(response => <BucketObject[]>response.json().objects);
  }

  saveBucketObject(bucketId: string, file: any): Observable<any> {
    let formData: FormData = new FormData();
    formData.append('bucket', bucketId);
    formData.append('file', file, file.name);
    return this.httpService.post(`${this.serverUrl}/buckets/${bucketId}/objects`, formData);
  }

  deleteBucketObject(bucketId: string, bucketObjectId: any): Observable<any> {
    return this.httpService.delete(`${this.serverUrl}/buckets/${bucketId}/objects/${bucketObjectId}`);
  }
}
