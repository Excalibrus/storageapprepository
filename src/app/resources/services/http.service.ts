import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, BaseRequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { AlertService } from './alert.service';

@Injectable()
export class HttpService extends Http {
  constructor (
      backend: XHRBackend,
      options: RequestOptions,
      private alertService: AlertService
      ) {

    super(backend, options);
  }

  request(url: string|Request, options?: RequestOptionsArgs): Observable<Response> {
    return super.request(url, options).catch(this.catchErrors(this));
  }

   private catchErrors(self: HttpService) {
    return (res: Response) => {
      if (res.status >= 400 || res.status <= 500) {
        let jsonResponse = res.json();
        if(jsonResponse.message) {
          let message = jsonResponse.message;
          if(res.status === 500) {
            message += ". Please refresh the page."
          }
          this.alertService.error(message);
        }
        else {
          this.alertService.error(res.text());
        }
      }
      else {
        this.alertService.error(res.toString());
      }
      return Observable.throw(res);
    };
  }
}

export class AuthRequestOptions extends BaseRequestOptions {
    token: string = "AFEA35BB-D1B0-4FA1-9DF8-7F958C55A5AE";
    constructor() {
        super();
        this.headers.append('Authorization', `Token ${this.token}`);
    }
}

