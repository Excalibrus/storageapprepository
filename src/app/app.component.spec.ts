/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { RoutingModule } from './routes/routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';

import { AppComponent } from './app.component';
import { BucketsComponent } from './pages/buckets/buckets.component';
import { BucketDetailsComponent } from './pages/bucket-details/bucket-details.component';
import { LocationsComponent } from './pages/locations/locations.component';
import { NewBucketComponent } from './pages/new-bucket/new-bucket.component';
import { AlertComponent } from './resources/components/alert/alert.component';

import { AlertService, BucketsService } from './resources/services/index';

describe('AppComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        BucketsComponent,
        BucketDetailsComponent,
        NewBucketComponent,
        LocationsComponent,
        AlertComponent
      ],
      imports: [
        RoutingModule,
        BrowserModule,
        FormsModule
      ],
       providers: [
        { provide: APP_BASE_HREF, useValue: '/' },
        AlertService,
        BucketsService
      ]
    });
    TestBed.compileComponents();
  });

  it('should create the app', async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
