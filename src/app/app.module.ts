import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, RequestOptions } from '@angular/http';
import { RouterModule } from '@angular/router';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { RoutingModule } from './routes/routing.module';
import { BucketsComponent } from './pages/buckets/buckets.component';
import { NewBucketComponent } from './pages/new-bucket/new-bucket.component';
import { AlertComponent } from './resources/components/alert/alert.component';
import { AlertService, HttpService, BucketsService, AuthRequestOptions } from './resources/services/index';

import { BucketDetailsComponent } from './pages/bucket-details/bucket-details.component';
import { LocationsComponent } from './pages/locations/locations.component';

@NgModule({
  declarations: [
    AppComponent,
    BucketsComponent,
    AlertComponent,
    NewBucketComponent,
    BucketDetailsComponent,
    LocationsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RoutingModule,
    RouterModule,
    NgbModule.forRoot()
  ],
  providers: [
    AlertService,
    HttpService,
    BucketsService,
    { provide: RequestOptions, useClass: AuthRequestOptions },
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
