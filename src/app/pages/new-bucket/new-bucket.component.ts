import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { BucketsService } from '../../resources/services/buckets.service';
import { Bucket, Location } from '../../resources/models/index';

@Component({
  selector: 'new-bucket',
  templateUrl: './new-bucket.component.html'
})
export class NewBucketComponent implements OnInit {
  name: string;
  locationId: string;
  locations: Location[];
  @Output() bucketConfirmed = new EventEmitter();

  constructor(private bucketsService: BucketsService) {  }

  ngOnInit() {
    this.getLocations();;
  }

  getLocations() {
    this.locations = new Array();
    this.bucketsService.getLocations().subscribe(response => {
      if(response) {
        this.locations = response;
      }
    });
  }

  createBucket() {
    if(this.name && this.name.length > 0 && this.locationId) {
      this.bucketsService.saveBucket(this.name, this.locationId).subscribe(response => {
        if(response) {
          this.bucketConfirmed.emit();
        }
      });
    }
  }
}
