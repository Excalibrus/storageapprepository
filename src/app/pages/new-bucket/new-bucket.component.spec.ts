/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { XHRBackend, BrowserXhr, ResponseOptions, Http, XSRFStrategy, RequestOptions } from '@angular/http';
import { RoutingModule } from '../../routes/routing.module';
import { APP_BASE_HREF } from '@angular/common';

import { NewBucketComponent } from './new-bucket.component';
import { BucketsComponent, BucketDetailsComponent, LocationsComponent } from '../index';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { BucketsService, HttpService, AlertService, AuthRequestOptions } from '../../resources/services/index';

describe('NewBucketComponent', () => {
  let component: NewBucketComponent;
  let fixture: ComponentFixture<NewBucketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NewBucketComponent,
        BucketsComponent,
        BucketDetailsComponent,
        LocationsComponent
      ],
      imports:[
        BrowserModule,
        FormsModule,
        RoutingModule
      ],
      providers: [
        BucketsService,
        HttpService,
        XHRBackend,
        BrowserXhr,
        Http,
        { provide: ResponseOptions},
        { provide: RequestOptions, useClass: AuthRequestOptions},
        XSRFStrategy,
        AlertService,
        { provide: APP_BASE_HREF, useValue: '/' },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBucketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
