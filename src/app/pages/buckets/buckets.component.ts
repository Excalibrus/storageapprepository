import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BucketsService } from '../../resources/services/index';
import { Bucket, Location } from '../../resources/models/index';

@Component({
  templateUrl: './buckets.component.html'
})
export class BucketsComponent implements OnInit {
  buckets: Bucket[];
  creatingNewBucket: boolean;

  constructor(
    private bucketsService: BucketsService,
    private router: Router) { }

  ngOnInit() {
    this.getBuckets();
    this.creatingNewBucket = false;
  }

  getBuckets() {
    this.buckets = new Array();
    this.bucketsService.getBuckets().subscribe(response => {
      if(response) {
        this.buckets = response;
      }
    });
  }

  bucketCreated() {
    this.creatingNewBucket = false;
    this.getBuckets();
  }

  navigateToBucketDetails(bucketId: string) {
    this.router.navigate(['buckets', bucketId]);
  }
}
