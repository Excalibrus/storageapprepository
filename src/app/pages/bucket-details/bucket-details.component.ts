import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BucketsService, AlertService } from '../../resources/services/index';
import { Bucket, BucketObject } from '../../resources/models/index';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  templateUrl: './bucket-details.component.html',
  styleUrls: ['./bucket-details.component.scss']
})
export class BucketDetailsComponent implements OnInit {
  sub: any;
  bucket: Bucket;
  objects: BucketObject[];
  @ViewChild('fileInput') fileInput:ElementRef;

  constructor(
    private bucketsService: BucketsService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.sub = this.activatedRoute.params.subscribe(params => {
       this.getBucket(params['id']);
    });
  }

  getBucket(bucketId: string) {
    this.bucketsService.getBucket(bucketId).subscribe(response => {
      if(response){
        this.bucket = response;
        this.getBucketObjects(this.bucket.id);
      }
    });
  }

  getBucketObjects(bucketId: string) {
    this.objects = new Array();
    this.bucketsService.getBucketObjects(bucketId).subscribe(response => {
      if(response){
        this.objects = response;
      }
    });
  }

  getFileSize(bytes: number) {
        if      (bytes>=1000000000) {return (bytes/1000000000).toFixed(2)+' GB';}
        else if (bytes>=1000000)    {return (bytes/1000000).toFixed(2)+' MB';}
        else if (bytes>=1000)       {return (bytes/1000).toFixed(2)+' KB';}
        else if (bytes>1)           {return bytes+' bytes';}
        else if (bytes==1)          {return bytes+' byte';}
        else                        {return '0 byte';}
  }

  deleteObject(object: BucketObject) {
    if(object) {
      this.bucketsService.deleteBucketObject(this.bucket.id, object.name).subscribe(response => {
        if(response && response.status === 200) {
          this.getBucketObjects(this.bucket.id);
        }
      });
    }
  }

  deleteBucket() {
    if(this.bucket) {
      this.bucketsService.deleteBucket(this.bucket.id).subscribe(response => {
        if(response && response.status === 200) {
          this.router.navigate(['buckets']);
        }
      });
    }
  }

  openDeleteObjectModal(modal: any, object: BucketObject) {
    this.modalService.open(modal).result.then((result) => {
      this.deleteObject(object);
    });
  }

  openDeleteBucketModal(modal: any) {
    this.modalService.open(modal).result.then((result) => {
      this.deleteBucket();
    });
  }

  onChange(event: any) {
    let files: File[] = event.srcElement.files;
    for(let file of files) {
      this.bucketsService.saveBucketObject(this.bucket.id, file).subscribe(response => {
        if(response && response.status === 201) {
          this.getBucketObjects(this.bucket.id);
        }
      })
    }
  }

}
