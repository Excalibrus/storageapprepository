/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { XHRBackend, BrowserXhr, ResponseOptions, Http, XSRFStrategy, RequestOptions } from '@angular/http';
import { RoutingModule } from '../../routes/routing.module';
import { APP_BASE_HREF } from '@angular/common';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { BucketsComponent, BucketDetailsComponent, LocationsComponent, NewBucketComponent } from '../index';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { BucketsService, HttpService, AlertService, AuthRequestOptions } from '../../resources/services/index';

describe('BucketDetailsComponent', () => {
  let component: BucketDetailsComponent;
  let fixture: ComponentFixture<BucketDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NewBucketComponent,
        BucketsComponent,
        BucketDetailsComponent,
        LocationsComponent
      ],
      imports:[
        BrowserModule,
        FormsModule,
        RoutingModule,
        NgbModule.forRoot()
      ],
      providers: [
        BucketsService,
        HttpService,
        XHRBackend,
        BrowserXhr,
        Http,
        { provide: ResponseOptions},
        { provide: RequestOptions, useClass: AuthRequestOptions},
        XSRFStrategy,
        AlertService,
        { provide: APP_BASE_HREF, useValue: '/' },
        NgbModal
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BucketDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
