import { Component, OnInit } from '@angular/core';
import { BucketsService } from '../../resources/services/index';
import { Location } from '../../resources/models/index';

@Component({
  templateUrl: './locations.component.html'
})
export class LocationsComponent implements OnInit {
  locations: Location[];

  constructor(private bucketsService: BucketsService) { }

  ngOnInit() {
    this.getLocations();
  }

  getLocations() {
    this.locations = new Array();
    this.bucketsService.getLocations().subscribe(response => {
      if(response) {
        this.locations = response;
      }
    });
  }

}
