import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BucketsComponent } from '../pages/buckets/buckets.component';
import { LocationsComponent } from '../pages/locations/locations.component';
import { BucketDetailsComponent } from '../pages/bucket-details/bucket-details.component';

const routes: Routes = [
  { path: 'buckets', component: BucketsComponent },
  { path: 'buckets/:id', component: BucketDetailsComponent },
  { path: 'locations', component: LocationsComponent },
  { path: '**', redirectTo: 'buckets', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class RoutingModule { }
