# StorageApp

Run `ng install` to install npm dependencies

Run `npm start` to start app using configured proxy.
Run `ng serve` to start app without proxy.

Run `ng test` to run unit tests.
